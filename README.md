## **SETON US MIGRATION**

**REGRESSION : LIST OF SCENARIOS**

* Add To Cart From Product Page
* Checkout As Guest
* Create Account
* Forgot Password
* Quick Order
* Quote Request


**UAT : LIST OF TICKETS**

* WPSMIG-67  - Quote request
* WPSMIG-92  - Product videos
* WPSMIG-95  - Social Media Tool(in product page)
* WPSMIG_101 - Live Chat
* WPSMIG-121 - Create Account
* WPSMIG-142 - Order History
* WPSMIG-148 - Apply Promo Code
* WPSMIG-155 - Checkout, AMEX Creadit Card
* WPSMIG-248 - Sitemap URL test
* WPSMIG-250 - Marin Implementation
* WPSMIG-253 - Google Remarketing, Comm Junction, Bing Adcenter
* WPSMIG-255 - 301 redirects


**HELPER**

* IMAP - checking of email in Gmail
* Mail Catcher - local email checking 
* Split URLs - using 'explode' to separe URLS (e.g. 301 redirect)
* Get Array from DB : getArrayFromDB() - Query from DB
* Change base URL : changeBaseURL()- can change base URL on the fly
* Check http_code : useCurl() - getting the headers of the URL and check the http_code and log result




**STATIC PAGE**

* Product Page
* Admin Page


**STEPS**

+ **Checkout**
* Billing

+ **User Steps**
* Login
* SignUp
* Live Chat
* Register




====================================================================
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact