<?php
namespace Codeception\Module;
use \Codeception\Module;
use WebGuy;
#use \Codeception\Module\Selenium2;

// here you can define custom functions for WebGuy 

class WebHelper extends \Codeception\Module
{

    public $separatedURLs=array();

    public function waitForUserInput($host,$username,$password){
        if(trim(fgets(fopen("php://stdin","r")))!= chr(13)) return;
    }


    public function splitTheURLs($url){
           $separatedURLs=explode(';',$url);
        return $separatedURLs;
    }


    #public function useIMAP($host,$username,$password){
    public function useIMAP($formValue){

         $host = '{imap.gmail.com:993/imap/ssl}INBOX';
         #$username = 'bradyqatest@gmail.com';
         $username = 'setonus.andromeda@gmail.com';
         $password = 'Setonusandromeda!';

        //$message=array();
        $message='';

        /* try to connect */
        $inbox = imap_open($host,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

        /* grab emails */
        $emails = imap_search($inbox,'UNSEEN FROM Custsvc_SetonUS');    //checking only the unread
        #$emails = imap_search($inbox,'ALL');
        #$emails = imap_search($inbox,'SUBJECT "Quote request" ');

        ///* if emails are returned, cycle through each...
        if($emails) {

           // /* put the newest emails on top
            rsort($emails);

            ///* for every email...
            foreach($emails as $email_number) {
                $message = quoted_printable_decode(imap_body($inbox,$email_number));
                //echo $message;
                $this->assertContains($formValue,$message);
            }
            //echo $output;
        }
        /* close the connection */
        imap_close($inbox);
        return $message;
    }



    public function getArrayFromDB($table, $column, $criteria = array()){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        $query.=' ORDER BY ENTITY_ID';

        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }

    public function getArrayFromDB_NoCondition($table, $column, $criteria = array()){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);

        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }


    //continue the query due to error
    public function getArrayFromDBLimit($table, $column, $criteria = array()){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        $query.=' ORDER BY ENTITY_ID ASC LIMIT 4406,75474';

        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }


    public function useCurl($url,$ent_id){
        /*
        //$url = "http://www.seton.com/dasd";
        $ch = curl_init ( $url );
        curl_setopt ( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9) Gecko/2008052906 Firefox/3.0" );
        curl_setopt ( $ch, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );

        curl_exec ( $ch );
        $result =  curl_getinfo ( $ch );
        curl_close ( $ch );

        //if($result['http_code'] != 200){
        $fp = fopen('tests\_data\URLTestResult_'.date("m_d").'.txt', 'a+');
        if ($ent_id == ' '){
            fwrite($fp, $result['http_code']." : ". $url."\r\n");
            fclose($fp);
        }else {
           fwrite($fp, "ENTITY_ID#". $ent_id."\r\n");
           fwrite($fp, $result['http_code']." : ". $url."\r\n");
           fclose($fp);
        }       //}
        //die();
        */

        try {
            $http_code = @get_headers($url, 1);
            //if($result['http_code'] != 200){

            $fp = fopen('tests\_data\URLTestResult_'.date("m_d").'.txt', 'a+');
            if ($ent_id == ' '){
                fwrite($fp, $http_code[0]." : ". $url."\r\n");
                fclose($fp);
            }else {
            fwrite($fp, "ENTITY_ID#". $ent_id."\r\n");
            fwrite($fp, $http_code[0] ." : ". $url."\r\n");
            fclose($fp);

            }       //}
            //die();
        }catch (\PHPUnit_Framework_AssertionFailedError $e){

            $fp = fopen('tests\_data\URLTestResultError_'.date("m_d").'.txt', 'a+');
            if ($ent_id == ' '){
                fwrite($fp, $http_code[0]." : ". $url."\r\n");
                fclose($fp);
            }else {
                fwrite($fp, "ENTITY_ID#". $ent_id."\r\n");
                fwrite($fp, $http_code[0] ." : ". $url."\r\n");
                fclose($fp);
            }  //}

        }//try & catch
        return true;

    }//function useCurl


/*  public function getChrome(){
        $this->getModule('Selenium2')->_reconfigure(array('browser' => 'chrome'));}
*/

    public function changeBaseURL($url){
        $this->getModule('WebDriver')->_reconfigure(array('url' => $url));
    }

    public function logTheResult($logFile,$expectedURL,$newURL,$ent_id){
        $fp = fopen('tests\_data\\'.$logFile.'_'.date("m_d").'.txt', 'a+');
        if ($ent_id == ' '){
            fwrite($fp, 'EXPECTED URL : '. $expectedURL."\r\n");
            fwrite($fp, 'NEW URL : '. $newURL."\r\n");
            fclose($fp);
        }else {
            fwrite($fp, "#". $ent_id."\r\n");
            fwrite($fp, 'EXPECTED URL : '. $expectedURL."\r\n");
            fwrite($fp, 'NEW URL : '. $newURL."\r\n");
            fclose($fp);

        }

    }






}
