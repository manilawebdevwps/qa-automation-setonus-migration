<?php

class ProductPage
{
    // include url of current page
    static $URL = '';
    static $ProductPageURL = '/danger-unauthroized-personnel-keep-out-no-admittance-signs-6154a.html';

    static $element_ButtonCart = '.btn-cart';
    static $element_SBWrapper = '#sb-wrapper';
    static $element_SBNavClose = '#sb-nav-close';
    static $element_SBProductImage = '.product-image';
    static $element_SBProductName = '.product-name';
    static $element_SBProductPrice = '.price';
    static $element_SBViewBasketButton = '.btn-green';
    static $element_SBContinueShoppingButton = '.button';

    static $element_CheckoutBasketTitle = '.page-title';
    static $element_CheckoutCartTable = '#shopping-cart-table';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
     public static function route($param)
     {
        return static::$URL.$param;
     }


product}