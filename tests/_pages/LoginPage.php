<?php

class LoginPage
{
    // include url of current page
    static $URL = 'index.php/zpanel/';
    static $URL_createAccount = '/customer/account/create/';
    static $BaseURL_https = 'https://na.qa.andromeda2.brady.nbs-test.com';

    static $usernameField = '#email';
    static $passwordField = '#pass';
    static $loginButton = "#send2";


    /////////////////////////////////////////////////////////////////////////////////
    //for testing purpose of Emakina while Beth has no access in Andromeda
    static $URL_admin = '';

    static $usernameField_admin = '#username';
    static $passwordField_admin = '#login';
    static $loginButton_admin = ".form-button";

    public static function route_admin($param)
    {
        return static::$URL_admin.$param;
    }
    ///////////////////////////////////////////////////////////////////////////////


    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }




}