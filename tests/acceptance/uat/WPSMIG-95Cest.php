<?php
namespace uat;
use \WebGuy;
use \Codeception\Module;
use Codeception\PHPUnit\Constraint\WebDriver;


class WPSMIG95Cest
{

    /*Social Media Tool (in product page)*/

    public function _before()
    {
    }

    public function _after()
    {
    }


    public static $url_product = '/parking-lot-security-safety-signs-security-notice-parking-lot-under-surveillance-l3042.html#19116B';
    public static $link_fb = '//form[@id="product_addtocart_form"]/div[2]/div[2]/div/div/div/a/span';
    //public static $link_printer = '//form[@id="product_addtocart_form"]/div[2]/div[2]/div/div/div/a[4]/span';
    public static $link_twitter   = '//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[2]/span';

    public static $username_fb = 'setonus.andromeda@gmail.com';
    public static $password_fb = 'Setonusandromeda!';
    public static $username_twitter = 'setonusandromed';
    public static $password_twitter = 'Setonusandromeda!';

    // tests
    public function socialMediaTool(WebGuy $I) {
        $I->wantTo('see the toolbox for social media');
        $I->expectTo('see the product page');
        $I->amOnPage(self::$url_product);
        $I->wait(3);
        $I->maximizeWindow();

        //*** Facebook **
       $I->expectTo('--- see the FB popup ---');
        $I->click(self::$link_fb);
        //$I->wait(5);
        $I->switchToWindow('facebook');
        $I->wait(5);
        $I->fillField('#email',self::$username_fb);
        $I->fillField('#pass',self::$password_fb);
        $I->click('Log In');
        $I->wait(5);
        $I->expectTo('see the product in the URL of Facebook');
        $I->seeInCurrentUrl('parking-lot-security-safety-signs-security-notice-parking-lot-under-surveillance-l3042.html');

        //*** Twitter ***
        $I->expectTo('--- see the twitter page ---');
        $I->switchToWindow();
        $I->click(self::$link_twitter);
        $I->wait(5);
        $I->switchToWindow('twitter_tweet');
        $I->fillField('#username_or_email',self::$username_twitter);
        $I->fillField('#password',self::$password_twitter);
        #$I->click('#u_0_1');
        $I->click('Sign in and Tweet');
        $I->wait(3);

        //*** Pinterest ***
        $I->expectTo('--- see the pinterest page ---');
        $I->switchToWindow();
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->moveMouseOver('#atic_pinterest_share');
        $I->click('#atic_pinterest_share');

        //*** StumbleUpon ***
        $I->expectTo('--- see the StumbleUpon page ---');
        $I->switchToWindow();
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->moveMouseOver('#atic_stumbleupon');
        $I->click('#atic_stumbleupon');
        $I->wait(10);

        //*** LinkedIn ***
        $I->expectTo('--- see the LinkedIn page ---');
        $I->switchToWindow();
        $I->wait(3);
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->expectTo('see the SECOND CLICK OF MOUSE');
        $I->wait(3);
        $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->switchToIFrame('at3winshare-iframe');
        $I->fillField('#service-filter','linkedIn');
        $I->click('.at3winsvc_linkedin');

        //*** Blogger ***
        $I->expectTo('--- see the Blogger page ---');
        $I->switchToWindow();
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
       // $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->switchToIFrame('at3winshare-iframe');
        $I->fillField('#service-filter','blogger');
        $I->wait(3);
        $I->click('.at3winsvc_blogger');

        //*** Wordpress ***
        $I->expectTo('--- see the Wordpress page ---');
        $I->switchToWindow(); //ENABLE THIS
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(3);
        $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->wait(10);
        //$I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[5]/span');
        $I->switchToIFrame('at3winshare-iframe');
        $I->fillField('#service-filter','wordpress');
        $I->click('.at3winsvc_wordpress');
        $I->wait(3);


        //*** Email ***
        $I->expectTo('--- see the Email pop-up ---');
        $I->switchToWindow();
        $I->moveMouseOver('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[3]/span');
        $I->wait(3);
        $I->click('//*[@id="product_addtocart_form"]/div[2]/div[2]/div[1]/div[1]/div[1]/a[3]/span');
        $I->wait(5);
        //limitation due to third party
        // http://www.addthis.com/tellfriend.php?v=300&winname=addthis&pub=ra-4dd0f91322cd0549&source=tbx-300&lng=en&s=email&url=http%3A%2F%2Fna.qa.andromeda2.brady.nbs-test.com%2Fparking-lot-security-safety-signs-security-notice-parking-lot-under-surveillance-l3042.html&title=Parking%20Lot%20Security%20%26%20Safety%20Signs%20-%20Security%20Notice%20Parking%20Lot%20Under%20Surveillance%20%7C%20Seton%20US&ate=AT-ra-4dd0f91322cd0549/-/-/5368e2c99c970f7d/8/525c2bf9200643d8&uid=525c2bf9200643d8&ct=1&ui_email_to=&ui_email_from=&ui_email_note=&tt=0&captcha_provider=nucaptcha&at3frame=true

        //*** Print ***
        $I->expectTo('--- see the Print pop-up ---');
        $I->wantTo('print the product page');
        $I->expectTo('see the pop-up of printer');
        $I->click('Share on print');
        $I->wait(5);








    }

}