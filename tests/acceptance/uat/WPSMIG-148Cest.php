<?php
namespace uat;
use \WebGuy;

class WPSMIG148Cest
{

    /* Applying promocode in a cart*/

    public function _before()
    {
    }

    public function _after()
    {
    }

    public static $product = '/polished-firefly-exit-signs-86875.html';
    public static $element_SBWrapper = '#sb-wrapper';
    public static $element_SBProductImage = '.product-image';
    public static $element_SBProductName = '.product-name';
    public static $element_SBProductPrice = '.price';
    public static $element_SBViewBasketButton = '.button.btn-green';
    public static $element_SBContinueShoppingButton = '.button';
    //public static $element_Cart_ProductID = '.product-ids';
    public static $element_Cart_couponcode = '#coupon_code';
    public static $element_Cart_orderSummary = '.totals.cart-block';
    public static $element_Cart_buttoncouponcode = '.button.apply-btn';
    public static $element_Cart_couponsuccessmsg = '.success-msg';
    public static $element_Cart_discounttext = "//*[@id='shopping-cart-totals-table']/tbody/tr[2]/th";

    public static $couponcode = 'JDEP66';

    private function truncateproductname($text) {
        $words=trim(substr($text, 0,strrpos(($text),'-')-3));
        return $words;
    }

    // tests
    public function createOrder(WebGuy $I) {
        $I->amOnPage(self::$product);
        $I->wantTo('Add product to cart');
        //$productID = $I->grabTextFrom(self::$element_Cart_ProductID);
        $grabbed = $I->grabTextFrom('h1');
        //$I->see($grabbed);
        $product = $this->truncateproductname($grabbed);
        //$product = trim(substr($grabbed, 0,strrpos($grabbed,'-')-3));
        $I->see($product);
        $I->click('Add to Cart');
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(self::$element_SBWrapper, 30); // secs
        $I->waitForElement(self::$element_SBProductImage, 10); // secs
        $I->expectTo('See Product image at the left');
        $I->seeElement(self::$element_SBProductImage);
        $I->expectTo('See Product name and code at the right');
        $I->seeElement(self::$element_SBProductName);
        $I->expectTo('See Product price at the right most');
        $I->seeElement(self::$element_SBProductPrice);
        $I->expectTo('See button for view basket');
        $I->wait(20);
        $I->switchToIFrame('sb-player');
        $I->seeElement(self::$element_SBViewBasketButton);
        $I->seeElement(self::$element_SBContinueShoppingButton);
        $I->wantTo('one page checkout');
        $I->expectTo('See cart page and its elements');
        $I->click(self::$element_SBViewBasketButton);
        $I->wait(20);
        $I->seeInCurrentUrl('checkout/cart/');
        $I->see($product);
        $I->seeElement(self::$element_Cart_couponcode);
        $I->seeElement(self::$element_Cart_orderSummary);
        $I->fillField(self::$element_Cart_couponcode,self::$couponcode);
        $I->click(self::$element_Cart_buttoncouponcode);
        $I->wait(5);
        $I->seeElement(self::$element_Cart_couponsuccessmsg);
        $I->see('Coupon code "'.self::$couponcode.'" was applied.');
        $I->see('Cart was updated');
        $I->see('Discount',self::$element_Cart_discounttext);
    }

}