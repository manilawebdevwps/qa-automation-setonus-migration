<?php
namespace uat;
use \WebGuy;

class WPSMIG253Cest
{

    /* Migrate Search Engine Remarketing Tags (Google Remarketing, Commission Junction, Bing Adcenter) */

    public function _before()
    {
    }


    public function _after()
    {
    }


    public $bingAdCenterTag = array(
        'AdCenter',
        '<script type="text/javascript">',
        'if (!window.mstag) mstag = {loadTag: function(){}, time: (new Date()).getTime()};',
        'id="mstag_tops"',
        'type="text/javascript"',
        'src="//flex.atdmt.com/mstag/site/dd83b3a3-e008-4ba3-bd11-3328339b4294/mstag.js"',
        'mstag.loadTag("analytics", {dedup:"1", domainId:"1309186", type:"1", revenue:"", actionid:"113640"})',
        'iframe src="//flex.atdmt.com/mstag/tag/dd83b3a3-e008-4ba3-bd11-3328339b4294/analytics.html?dedup=1&',
        'domainId=1309186&',
        'type=1&',
        'revenue=&',
        'actionid=113640"',
        'frameborder="0" scrolling="no" width="1" height="1" style"visibility:hidden;display:none"',
        '/AdCenter'
    );

    public $cjTag = array(
        '<!-- BEGIN COMMISSION JUNCTION TRACKING CODE – DO NOT MODIFY -->',
        'iframe',
        'height="1"',
        'width="1"',
        'frameborder="0"',
        'scrolling="no"',
        'src="https://www.emjcd.com/tags/c?containerTagid=4811&',
        'CID=1514994&',
        'TYPE=331348&',
        //'OID=WS30001469&',      //OrderID
        //'AMOUNT=10.5400&',
        'CURRENCY=USD&',
        'DISCOUNT=0.0000&',
        //'ITEM1=97759&',
        //'AMT1=0.0000&',
        //'QTY1=1.0000&',
        'name="cj_conversion"',
        '<!-- END COMMISSION JUNCTION TRACKING CODE -->'
    );

    public $dynamicRemarketingTag = array(
        '<!-- Dynamic Remarketing Ads Google Tag - Seton US -->',
        '<!-- ------------------------------------------------------',
        'PLEASE INCLUDE THIS COMMENT ON THE WEB PAGE WITH THE TAG',
        'Remarketing tags may not be associated with personally identifiable information',
        'or placed on pages related to sensitive categories. For more information on',
        'this, see http://google.com/ads/remarketingsetup',
        '------------------------------------------------------ -->',
        '<script type="text/javascript">',
        'var google_tag_params = {',
        //'ecomm_pagetype: "'.$this->pageType.'"',
        //'ecomm_pcat: \'\'',
        //'ecomm_prodid: \'\'',
        //'ecomm_pvalue: \'\'',
        //'ecomm_pname: \'\'',
        //'ecomm_totalvalue: \'\'',
        '</script>',
        ' <script type="text/javascript">',
        //'/* <![CDATA[ */',
        'var google_conversion_id = 1050357188;',
        'var google_conversion_label = "jo8BCMrerwQQxNvs9AM";',
        'var google_custom_params = window.google_tag_params;',
        'var google_remarketing_only = true;',
        //'/* ]]> */',
        '</script>',
        //'<div style=\'display:none\' id="50a32bf164214cf4bac10458b84b483d"></div>',
        'id="50a32bf164214cf4bac10458b84b483d"',
        'if (typeof(__$1D3F) == \'undefined\') {__$1D3F = { deferred: [], deferScript: function(id,d,s) { __$1D3F.deferred.push( [id,d,s] ); }',
        '__$1D3F.deferScript(\'50a32bf164214cf4bac10458b84b483d\', \'//www.googleadservices.com/pagead/conversion.js\');',
        //'sendGoogleRemarketingTag();',       ///added but not in the script
        '</script>',
        '<noscript>',
        'div style="display:inline;"',
        //'<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1050357188/?value=0&amp;label=jo8BCMrerwQQxNvs9AM&amp;guid=ON&amp;script=0" />',
        'img height="1" width="1" style="border-style:none;" alt=""',
        'googleads.g.doubleclick.net/pagead/viewthroughconversion/1050357188/?value=0&amp;',
        'label=jo8BCMrerwQQxNvs9AM&amp',
        'guid=ON&amp',
        '</div>',
        '</noscript>',
        '<!-- / Dynamic Remarketing Ads Google Tag - Seton US -->'
    );


    public static $categoryPage = '/asset-tags/asset-id-tags-bar-code/non-bar-coded.html';
    public static $productPage = 'safety-security/badges-id-management/badge-lanyards-reels/swivel-back-badge-reel-jm145.html#97759';
    public static $ProductPage_2 = 'hazwaste-drum-labels-on-roll-used-absorbents-97720.html';
    public static $element_SBWrapper = '#sb-wrapper';
    public static $element_SBProductImage = '.product-image';
    public static $element_SBProductName = '.product-name';
    public static $element_SBProductPrice = '.price';
    public static $element_SBViewBasketButton = '.button.btn-green';
    public static $element_SBContinueShoppingButton = '.button';
    public static $element_Cart_couponcode = '#coupon_code';
    public static $element_Cart_orderSummary = '.totals.cart-block';
    public static $checkout_proceedcheckout = '.button.btn-proceed-checkout.btn-checkout';
    public static $usernameField = '#login-email';
    public static $passwordField = '#login-password';
    public static $loginButton = '//*[@id="login-form"]/div/button';
    public static $username = 'lilybeth_ranada@bradycorp.com';
    public static $password = 'lilybeth';
    public static $checkout_stepbilling = '#checkout-step-billing';
    public static $checkout_billingshiptothisadd = '#billing:use_for_shipping_yes';
    public static $checkout_billingbutton = '//*[@id="billing-buttons-container"]/button';
    public static $checkout_shippingmethodbutton = '//*[@id="shipping-method-buttons-container"]/button';
    public static $checkout_stepshippingmethod = '#opc-shipping_method';
    public static $checkout_shippingbestwayground = '#matrixrate';
    public static $checkout_steppaymentinformation = '#checkout-step-payment';
    public static $checkout_invoiceme = '#p_method_purchaseorder';
    public static $checkout_elementorderreview = '#checkout-review-table-wrapper';
    public static $checkout_paymentinformationbutton = '//*[@id="payment-buttons-container"]/button';
    public static $checkout_orderreviewbutton = '//*[@id="review-buttons-container"]/button';
    //public static $checkout_orderreceived = 'html/body/div[3]/div[3]/div[2]/div/div[1]/p[1]';
    public static $checkout_orderreceived = 'html/body/div[3]/div[3]/div[2]/div/div[1]/p[1]/a';
    public static $checkout_grandTotal= '//*[@id="checkout-onepage-totals-table"]/tfoot/tr/td[2]/strong/span';



    // tests
 /*   public function googleRemarketingTagHomePage(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('***** see the jsscript of Marin in the homepage *****');
        //homepage test of Google Remarketing
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //QA is not updated with remarketing tags
        $I->amOnPage('/');
        $I->see('ecomm_pagetype: \'home\'');
        //$I->see('ecomm_pcat: \'\'');
        //$I->see('ecomm_prodid: \'\'');
        //$I->see('ecomm_pvalue: \'\'');
        //$I->see('ecomm_pname: \'\'');
        //$I->see('ecomm_totalvalue: \'\'');
        foreach ($this->dynamicRemarketingTag as $value){
            //$I->canSee($value);
            $I->see($value);
        }
    }

        //search Page test of Google Remarketing Tag
     public function googleRemarketingTagSearchResult(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('***** see the jsscript of Marin in search result page *****');
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //QA is not updated with remarketing tags
        $I->amOnPage('/');
        $I->fillField('#search','baggage');
        $I->wait(5);
        $I->click('.button');
        $I->wait(20);
        $I->see('ecomm_pagetype: \'searchresults\'');
        //$I->see('ecomm_pcat: \'\'');
        //$I->see('ecomm_prodid: \'\'');
        //$I->see('ecomm_pvalue: \'\'');
        //$I->see('ecomm_pname: \'\'');
        //$I->see('ecomm_totalvalue: \'\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }
     }

     //product page test of Google remarketing
    public function googleRemarketingTagProductPage(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('***** see the jsscript of Marin in search product page *****');
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //QA is not updated with remarketing tags
        $I->amOnPage(self::$productPage);
        $I->wait(25);
        $I->see('ecomm_pagetype: \'product\'');
        //$I->canSee('ecomm_pcat: \'Badge Lanyards & Reels\'');
        $I->canSee('ecomm_prodid: \'JM145\'');
        $I->see('ecomm_pvalue: \'4.2000\'');
        $I->see('ecomm_pname: \'Swivel Back Badge Reel\'');
        //$I->see('ecomm_totalvalue: \'\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }
    }

      //category page test of Google remarketing
    public function googleRemarketingTagCategorypage(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('***** see the jsscript of Marin in category page *****');
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //QA is not updated with remarketing tags
        $I->amOnPage(self::$categoryPage);
        $I->see('ecomm_pagetype: \'category\'');
        $I->see('ecomm_pcat: \'Non Bar Coded Asset Tags\'');
        //$I->see('ecomm_prodid: \'\'');
        //$I->see('ecomm_pvalue: \'\'');
        //$I->see('ecomm_pname: \'\'');
        //$I->see('ecomm_totalvalue: \'\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }
    }

*/
   public function googleRemarketingTagCartPage(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('***** see the jscript of Marin in cart page *****');
        //cart page - do logged-in
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //QA is not updated with remarketing tags
        $I->amOnPage(self::$productPage);
        $I->wait(20);
        $I->click('#button-cart');
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(self::$element_SBWrapper, 30); // secs
        $I->waitForElement(self::$element_SBProductImage, 10); // secs
        $I->expectTo('See Product image at the left');
        $I->seeElement(self::$element_SBProductImage);
        $I->expectTo('See Product name and code at the right');
        $I->seeElement(self::$element_SBProductName);
        $I->expectTo('See Product price at the right most');
        $I->seeElement(self::$element_SBProductPrice);
        $I->expectTo('See button for view basket');
        $I->wait(25);
        $I->switchToIFrame('sb-player');
        $I->wait(25);
        $I->seeElement(self::$element_SBViewBasketButton);
        $I->seeElement(self::$element_SBContinueShoppingButton);
        $I->wantTo('one page checkout');
        $I->expectTo('See cart page and its elements');
        $I->click(self::$element_SBViewBasketButton);
        //$I->seeInCurrentUrl('checkout/onepage/'); //QA
        $I->seeInCurrentUrl('checkout/cart/');    //Staging
        $I->wait(25);
        //$I->seeElement(self::$element_Cart_orderSummary);
        $I->expectTo('see remarketing values in the cart page');
        $I->see('ecomm_pagetype: \'cart\'');
        //$I->canSee('ecomm_pcat: [\'Badge Lanyards & Reels\']');
        $I->canSee('ecomm_prodid: [\'97759\']');
        $I->see('ecomm_pvalue: [\'4.2\']');
        $I->see('ecomm_pname: [\'Swivel Back Badge Reel\']');
        $I->see('ecomm_totalvalue: \'10.44\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }
        //add another product
        $I->expectTo('***** see the group products in the cart page *****');
        $I->amOnPage(self::$ProductPage_2);
        $I->wait(25);
        $I->click('.button.btn-cart');
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(self::$element_SBWrapper, 30); // secs
        $I->waitForElement(self::$element_SBProductImage, 10); // secs
        $I->expectTo('See Product image at the left');
        $I->seeElement(self::$element_SBProductImage);
        $I->expectTo('See Product name and code at the right');
        $I->seeElement(self::$element_SBProductName);
        $I->expectTo('See Product price at the right most');
        $I->seeElement(self::$element_SBProductPrice);
        $I->expectTo('See button for view basket');
        $I->wait(25);
        $I->switchToIFrame('sb-player');
        $I->seeElement(self::$element_SBViewBasketButton);
        $I->seeElement(self::$element_SBContinueShoppingButton);
        $I->wantTo('one page checkout');
        $I->expectTo('See cart page and its elements');
        $I->click(self::$element_SBViewBasketButton);
        //$I->seeInCurrentUrl('checkout/onepage/'); //QA
        $I->seeInCurrentUrl('checkout/cart/');    //Staging
        $I->wait(25);
        //$I->seeElement(self::$element_Cart_orderSummary);
        $I->expectTo('see remarketing values in the cart page');
        $I->see('ecomm_pagetype: \'cart\'');
        //$I->see('ecomm_pcat: [\'Badge Lanyards & Reels\',\'Chemical Hazardous Labels\']');
        $I->see('ecomm_prodid: [\'97759\',\'97720\']');
        $I->see('ecomm_pvalue: [\'4.2\',\'315.3\']');
        //$I->see('ecomm_pname: [\'Swivel Back Badge Reel\',\'Hazwaste &#38; Drum Labels-On-A-Roll - Used Absorbents\']');  //- FOR MODIFICATION
        $I->see('ecomm_pname: [\'Swivel Back Badge Reel\',');
        $I->see('\'Hazwaste &amp;');
        $I->see('Drum Labels-On-A-Roll - Used Absorbents\']');
        $I->see('ecomm_totalvalue: \'358.12\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }

        //Success Page Google remarketing
        $I->wantTo('one page checkout');
        $I->expectTo('proceed checkout');
        $I->click(self::$checkout_proceedcheckout);
        $I->expectTo('See login page');
        $I->seeInCurrentUrl('checkout/onepage/');
        $I->waitForElement(self::$usernameField, 30);
        $I->waitForElement(self::$loginButton, 30); // secs
        $I->fillField(self::$usernameField,self::$username);
        $I->fillField(self::$passwordField,self::$password);
        $I->click(self::$loginButton);
        $I->expectTo('See checkout one page');
        $I->seeInCurrentUrl('checkout/onepage/index/');
        $I->expectTo('see billing section');
        $I->wait(10);
        $I->seeElement(self::$checkout_stepbilling);
        $I->seeElement(self::$checkout_billingshiptothisadd);
        $I->seeElement(self::$checkout_billingbutton);
        $I->click(self::$checkout_billingshiptothisadd);
        $I->wait(3);
        $I->click(self::$checkout_billingbutton);
        $I->wait(5);
        $I->expectTo('see shipping method section');
        $I->seeElement(self::$checkout_stepshippingmethod);
        $I->wait(5);
        $I->click(self::$checkout_shippingbestwayground);
        $I->click(self::$checkout_shippingmethodbutton);
        $I->wait(10);
        $I->expectTo('see payment information section');
        $I->seeElement(self::$checkout_steppaymentinformation);
        $I->click(self::$checkout_invoiceme);
        $I->click(self::$checkout_paymentinformationbutton);
        $I->expectTo('see order review section');
        $I->wait(5);
        $I->seeElement(self::$checkout_elementorderreview);
        $grandTotal = $I->grabTextFrom(self::$checkout_grandTotal);
        $I->click(self::$checkout_orderreviewbutton);
        $I->wait(5);
        $I->canSeeInCurrentUrl('checkout/onepage/success/');
        //$orderIDLine = $I->grabTextFrom(self::$checkout_orderreceived);
        $orderID = $I->grabTextFrom(self::$checkout_orderreceived);
        //$orderIDLineExploded =explode(':',$orderIDLine);
        //$trimmed_orderIDLineExploded=trim($orderIDLineExploded[1]);
        //$orderID=$trimmed_orderIDLineExploded.substr(0,10);
        //$orderID2=$trimmed_orderIDLineExploded.substr(1,10);
        $I->wait(5);
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('see the jscript of Marin is implemented in success page');
        $I->wait(5);
        $I->see('ecomm_pagetype: \'purchase\'');                                                 //for jerome's modification
        $I->see('ecomm_pcat: [\'Badge Lanyards');
        $I->see('\'Labels\']');                                               //for jerome's modification
        $I->see('ecomm_prodid: [\'97759\',\'97720\']');                                          //for jerome's modification
        $I->see('ecomm_pvalue: [\'4.2\',\'315.3\']');
        //$I->see('ecomm_pname: [\'Swivel Back Badge Reel\',\'Hazwaste &#38; Drum Labels-On-A-Roll - Used Absorbents\']');  //- FOR MODIFICATION
        $I->see('ecomm_pname: [\'Swivel Back Badge Reel\',');
        $I->see('\'Hazwaste &amp;');
        $I->see('Drum Labels-On-A-Roll - Used Absorbents\']');
        $I->see('ecomm_totalvalue: \'365.7100\'');
        foreach ($this->dynamicRemarketingTag as $value){
            $I->see($value);
        }

       //Bing on Thank You Page
       $I->wantTo('check the jscript of AdCenter');
       $I->expectTo('***** see the jscript of BING / ADCENTER is implemented in success page *****');
       foreach ($this->bingAdCenterTag as $value){
           $I->see($value);
       }


       //CJ on Thank You Page
       $I->wantTo('check the jscript of AdCenter');
       $I->expectTo('***** see the jscript of COMMISSION JUNCTION is implemented in success page *****');
       $I->see('OID='.$orderID.'');
       $I->see('ITEM2=97720');
       $I->see('AMT2=315.3000');
       $I->see('QTY2=1.0000');
       $I->see('ITEM1=97759');
       $I->see('AMT1=4.2000');
       $I->see('QTY1=1.0000');
       foreach ($this->cjTag as $value){
           $I->see($value);
       }

    }



}