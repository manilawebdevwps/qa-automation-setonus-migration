<?php
namespace uat;
use \WebGuy;
use \Codeception\Module\WebHelper;

class WPSMIG255Cest extends \Codeception\Module
{

    /********************************************************************************************************/
    /*  Generate 301 Directs                                                                                */
    /*  Log the FAIL result in C:\xampp\htdocs\repo\qa-automation-setonus-migration\tests\_data             */
    /*  Note : grabFromCurrentUrl - add '/' on the grab text                                                */
    /********************************************************************************************************/


    public function _before()
    {
    }

    public function _after()
    {
    }


    public static $oldURL;

    // tests
    public function test301URLs(WebGuy $I) {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of 301 redirects');
        $counter = 0;
        $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com/');
        $logFile = '301RedirectsFAIL';
        $results = $I->getArrayFromDB_NoCondition('301Directs','url',array());
        foreach ($results as $key=>$val) {
           $I->expectTo('**********  '.$counter++.'  **********');
           $separatedURL = $I->splitTheURLs($val['url']);
           $newURL = '/'.$separatedURL[1];
           $I->amOnPage($separatedURL[0]);
           $I->wait(5);
           $expectedURL = $I->grabFromCurrentUrl();
           $I->expectTo('see expected URL : '.$expectedURL);
           $I->expectTo('see New URL      : '.$newURL);
           if (trim($expectedURL) == trim($newURL)){
               $I->expectTo('OK');
           }else{
                $I->logTheResult($logFile,$expectedURL,$newURL,$counter);
           }
        }

    
    }

}