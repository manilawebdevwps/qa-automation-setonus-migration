<?php
namespace uat;
use \WebGuy;
use Faker\Factory;

class WPSMIG250Cest
{

    /* Seton US: Marin implementation */



    public function _before()
    {
    }

    public function _after()
    {
    }


    public $marinCodeLandingPage = array(
        '<!-- BEGIN: Marin Software Tracking Script (Landing Page) -->',
        '<script type="text/javascript">',
        'var _mTrack = _mTrack || [];',
        '_mTrack.push([\'trackPage\']);',
        '(function() {',
        'var mClientId = \'6ifob149j0\';',
        'var mProto = ((\'https:\' == document.location.protocol) ? \'https://\' : \'http://\');',
        'var mHost = \'tracker.marinsm.com\';',
        'var mt = document.createElement(\'script\'); mt.type = \'text/javascript\'; mt.async = true; mt.src = mProto + mHost + \'/tracker/async/\' + mClientId + \'.js\';',
        'var mt = document.createElement(\'script\'); mt.type = \'text/javascript\'; mt.async = true; mt.src = mProto + mHost + \'/tracker/async/\' + mClientId + \'.js\';',
        'var fscr = document.getElementsByTagName(\'script\')[0]; fscr.parentNode.insertBefore(mt, fscr);',
        '})();',
        '</script>',
        '<noscript>',
       //'<img width="1" height="1" src="https://tracker.marinsm.com/tp?act=1&cid=6ifob149j0&script=no" />',
        '</noscript>',
        '<!-- END: Copyright Marin Software -->'
        );

    public static $categoryPage = '/tapes/shipping-packing-tapes/packing-carton-sealing';
    public static $productPage = '/safety-security/badges-id-management/badge-lanyards-reels/swivel-back-badge-reel-jm145.html#97759';
    public static $element_SBWrapper = '#sb-wrapper';
    public static $element_SBProductImage = '.product-image';
    public static $element_SBProductName = '.product-name';
    public static $element_SBProductPrice = '.price';
    public static $element_SBViewBasketButton = '.button.btn-green';
    public static $element_SBContinueShoppingButton = '.button';
    public static $element_Cart_couponcode = '#coupon_code';
    public static $element_Cart_orderSummary = '.totals.cart-block';
    public static $checkout_proceedcheckout = '.button.btn-proceed-checkout.btn-checkout';
    public static $usernameField = '#login-email';
    public static $passwordField = '#login-password';
    public static $loginButton = '//*[@id="login-form"]/div/button';
    public static $username = 'lilybeth_ranada@bradycorp.com';
    public static $password = 'lilybeth';
    public static $checkout_stepbilling = '#checkout-step-billing';
    public static $checkout_billingshiptothisadd = '#billing:use_for_shipping_yes';
    public static $checkout_billingbutton = '//*[@id="billing-buttons-container"]/button';
    public static $checkout_shippingmethodbutton = '//*[@id="shipping-method-buttons-container"]/button';
    public static $checkout_stepshippingmethod = '#opc-shipping_method';
    public static $checkout_shippingbestwayground = '#matrixrate';
    public static $checkout_steppaymentinformation = '#checkout-step-payment';
    public static $checkout_invoiceme = '#p_method_purchaseorder';
    public static $checkout_elementorderreview = '#checkout-review-table-wrapper';
    public static $checkout_paymentinformationbutton = '//*[@id="payment-buttons-container"]/button';
    public static $checkout_orderreviewbutton = '//*[@id="review-buttons-container"]/button';
    public static $checkout_orderreceived = 'html/body/div[1]/div[3]/div[2]/div/div[1]/p[1]/a';
    public static $checkout_grandTotal= '//*[@id="checkout-onepage-totals-table"]/tfoot/tr/td[2]/strong/span';

    // tests
      public function marinImplementationLandingPage(WebGuy $I) {

        $I->wantTo('see the script of Marin in the homepage');
         $I->expectTo('***** check the landing page jscript of Marin in the homepage *****');
         //homepage test of Marin
         $I->amOnPage('/');
         foreach ($this->marinCodeLandingPage as $value){
             $I->see($value);
         }

         //search Page test of Marin
         $I->wantTo('see the script of Marin in the search page');
         $I->expectTo('***** check the landing page jscript of Marin in the search page *****');
         //$I->amOnPage('/');
         $I->fillField('#search','baggage');
         $I->click('.button');
         foreach ($this->marinCodeLandingPage as $value){
             $I->canSee($value);
         }


         //category page test of Marin
         $I->wantTo('see the script of Marin in the category page');
         $I->expectTo('***** check the landing page jscript of Marin in the category page *****');
         $I->amOnPage(self::$categoryPage);
         foreach ($this->marinCodeLandingPage as $value){
             $I->see($value);
         }

                   //HAVING AN ERROR IN PRODUCT PAGE OF QA > moved to staging test
         //product page test of Marin
         $I->wantTo('see the script of Marin in the product page');
         $I->expectTo('***** check the landing page jscript of Marin in the product page *****');
         $I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');   //error in qa platform for product page
         //$I->amOnPage('/');
         $I->amOnPage(self::$productPage);
         foreach ($this->marinCodeLandingPage as $value){
             $I->see($value);
         }

         //cart page - do logged-in
         $I->wantTo('see the script of Marin in the cart page');
         $I->expectTo('***** check the landing page jscript of Marin in the cart page *****');
         $I->click('Add to Cart');
         $I->expectTo('See overlay is displayed');
         $I->waitForElement(self::$element_SBWrapper, 30); // secs
         $I->waitForElement(self::$element_SBProductImage, 10); // secs
         $I->expectTo('See Product image at the left');
         $I->seeElement(self::$element_SBProductImage);
         $I->expectTo('See Product name and code at the right');
         $I->seeElement(self::$element_SBProductName);
         $I->expectTo('See Product price at the right most');
         $I->seeElement(self::$element_SBProductPrice);
         $I->expectTo('See button for view basket');
         $I->wait(10);
         $I->switchToIFrame('sb-player');
         $I->seeElement(self::$element_SBViewBasketButton);
         $I->seeElement(self::$element_SBContinueShoppingButton);
         $I->wantTo('one page checkout');
         $I->expectTo('See cart page and its elements');
         $I->click(self::$element_SBViewBasketButton);
         $I->wait(10);
         //$I->seeInCurrentUrl('checkout/onepage/'); //QA
         $I->seeInCurrentUrl('checkout/cart/');    //Staging
         $I->seeElement('.cart');
         $I->seeElement(self::$element_Cart_orderSummary);
         foreach ($this->marinCodeLandingPage as $value){
             $I->see($value);
         }
         //Success Page Marin Implementation
         $I->wantTo('one page checkout');
         $I->expectTo('proceed checkout');
         $I->click(self::$checkout_proceedcheckout);
         $I->expectTo('See login page');
         $I->seeInCurrentUrl('checkout/onepage/');
         $I->waitForElement(self::$usernameField, 30); // secs
         $I->waitForElement(self::$passwordField, 10); // secs
         $I->waitForElement(self::$loginButton, 30); // secs
         $I->fillField(self::$usernameField,self::$username);
         $I->fillField(self::$passwordField,self::$password);
         $I->click(self::$loginButton);
         $I->expectTo('See checkout one page');
         $I->seeInCurrentUrl('checkout/onepage/index/');
         $I->expectTo('see billing section');
         $I->seeElement(self::$checkout_stepbilling);
         $I->seeElement(self::$checkout_billingshiptothisadd);
         $I->seeElement(self::$checkout_billingbutton);
         $I->click(self::$checkout_billingshiptothisadd);
         $I->wait(3);
         $I->click(self::$checkout_billingbutton);
         $I->wait(5);
         $I->expectTo('see shipping method section');
         $I->seeElement(self::$checkout_stepshippingmethod);
         $I->wait(5);
         $I->click(self::$checkout_shippingbestwayground);
         $I->click(self::$checkout_shippingmethodbutton);
         $I->wait(10);
         $I->expectTo('see payment information section');
         $I->seeElement(self::$checkout_steppaymentinformation);
         $I->click(self::$checkout_invoiceme);
         $I->click(self::$checkout_paymentinformationbutton);
         $I->expectTo('see order review section');
         $I->wait(5);
         $I->seeElement(self::$checkout_elementorderreview);
         $grandTotal = $I->grabTextFrom(self::$checkout_grandTotal);
         $grandTotalTrimmed=substr($grandTotal, 1,strlen($grandTotal));

         $I->click(self::$checkout_orderreviewbutton);
         $I->wait(10);
         $I->canSeeInCurrentUrl('checkout/onepage/success/');
         $I->wantTo('check the jscript of Marin');
         $I->expectTo('see the jscript of Marin is implemented');
         $orderID = $I->grabTextFrom(self::$checkout_orderreceived);
         $I->see('<!-- BEGIN: Marin Software Tracking Script (Confirmation Page) -->');
         $I->see(' <script type="text/javascript">');
         $I->see('var _mTrack = _mTrack || [];');
         $I->see('_mTrack.push([\'addTrans\', {');
         $I->see('currency: \'USD\',');
         $I->see('items: [');
         $I->see('orderId: \''.$orderID.'\',');
         $I->see('convType: \'ecom\',');
         $I->see('price: \''.$grandTotalTrimmed.'00'.'\'');
         $I->see(']');
         $I->see('}]);');
         $I->see('_mTrack.push([\'processOrders\']);');
         $I->see('(function() {');
         $I->see('var mClientId = \'6ifob149j0\';');
         $I->see('var mProto = ((\'https:\' == document.location.protocol) ? \'https://\' : \'http://\');');
         $I->see('var mHost = \'tracker.marinsm.com\';');
         $I->see('var mt = document.createElement(\'script\'); mt.type = \'text/javascript\'; mt.async = true; mt.src = mProto + mHost + \'/tracker/async/\' + mClientId + \'.js\';');
         $I->see('var fscr = document.getElementsByTagName(\'script\')[0]; fscr.parentNode.insertBefore(mt, fscr);');
         $I->see('})();');
         $I->see('</script>');
         $I->see('<noscript>');
         //$I->see('<img width=\"1\" height=\"1\" src=\"https://tracker.marinsm.com/tp?act=2&cid=6ifob149j0&script=no\" />');
         $I->see('</noscript>');
         $I->see('<!-- END: Copyright Marin Software -->');
     }


    public function marinImplementationCatalogRequestPage(WebGuy $I) {
        $I->wantTo('check the jscript of Marin');
        $I->expectTo('see the jsscript of Marin in catalog request');
        //$I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com/catalog/request/');
        $I->amOnPage('/');
        $I->click('Request a Catalog & Samples');
        $I->wait(5);
        //$I->amOnPage('/quote/request/');
        $faker = Factory::create();
        //firstname
        $I->fillField('#firstname',$faker->firstName);
        //lastname
        $I->fillField('#lastname',$faker->lastName);
        //Company
        $I->expectTo('see Company field is filled-up');
        $I->fillField('#company',$faker->company);
        //Email address
        $I->expectTo('see Email address field is filled-up');
        $I->fillField('#email',$faker->email);
        //Phone number
        $I->wantTo('fill fields of contact number');
        //$I->fillField('#telephone',$faker->phoneNumber);
        $I->fillField('#telephone','123-456-7890');
        //Street Address 1
        $I->expectTo('see Street Address field is filled-up');
        $I->fillField('#street1',$faker->streetAddress);
         //City
        $I->expectTo('see City field is filled-up');
        $I->fillField('#city',$faker->city);
        //State/Province
        $I->expectTo('see State/Province');
        $I->selectOption('form select[name=region_id]', 'New York');
        //Zip code
        $I->expectTo('see Postcode field is filled-up');
        $I->fillField('#zip',$faker->postcode);
        //Submit request
        $I->wantTo('send now the quote to  Customer Quoting Group ');
        $I->expectTo('Submit the form');
        $I->click('Submit Request');
        $I->wait(5);
        $I->wantTo('check the jscript of Marin in catalog request page');
        $I->expectTo('see the implementation of Marin in catalog request');
        $I->See('<!-- BEGIN: Marin Software Tracking Script (Confirmation Page) -->');
        $I->See('<script type="text/javascript">');
        $I->See('var _mTrack = _mTrack || [];');
        $I->See('_mTrack.push([\'addTrans\', {');
        $I->See('currency :\'USD\',');
        $I->See('items : [');
        $I->See('convType : \'catrequest\'');
        $I->See('}]');
        $I->See('}]);');
        $I->See('_mTrack.push([\'processOrders\']);');
        $I->See('(function() {');
        $I->See('var mClientId = \'6ifob149j0\';');
        $I->See('var mProto = ((\'https:\' == document.location.protocol) ? \'https://\' : \'http://\');');
        $I->See('var mHost = \'tracker.marinsm.com\';');
        $I->See('var mt = document.createElement(\'script\'); mt.type = \'text/javascript\'; mt.async = true; mt.src = mProto + mHost + \'/tracker/async/\' + mClientId + \'.js\';');
        $I->See('var fscr = document.getElementsByTagName(\'script\')[0]; fscr.parentNode.insertBefore(mt, fscr);');
        $I->See('})();');
        $I->See('</script>');
        $I->See('<noscript>');
        //$I->canSee('<img width="1" height="1" src="https://tracker.marinsm.com/tp?act=2&cid=6ifob149j0&script=no" />');
        $I->See('</noscript>');
        $I->See('<!-- END: Copyright Marin Software -->');
        $I->expectTo('***** see also the landing page in catalog request page *****');
        foreach ($this->marinCodeLandingPage as $value){
            $I->see($value);
        }
    }


}