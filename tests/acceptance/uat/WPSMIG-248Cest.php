<?php
namespace uat;
use \WebGuy;

class WPSMIG248Cest
{

    /********************************************************************************************************/
    /*  Sitemap creation                                                                                    */
    /*  convert xml to csv file (http://localhost/repo/SiteMapXML/xml_to_csv.php?f=sitemap-Andromeda.xml)   */
    /*  Upload to DB                                                                                        */
    /********************************************************************************************************/


    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function testSiteMapURLs(WebGuy $I) {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of sitemap ');
        $counter = 0;
        $results = $I->getArrayFromDB_NoCondition('sitemap','url',array());
        foreach ($results as $key=>$val) {
            $counter++;
            $I->useCurl($val['url'],$counter);
        }
    }

}