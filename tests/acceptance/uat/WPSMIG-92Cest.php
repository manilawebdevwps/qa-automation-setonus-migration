<?php
namespace uat;
use \WebGuy;

class WPSMIG92Cest
{

    /* Product Videos */

    public function _before()
    {
    }

    public function _after()
    {
    }



    public $arrlength= '';

    public $productpage = array(
        array('http://na.qa.andromeda2.brady.nbs-test.com/bulk-arrow-signs-right-2345c.html','videos.treepodia.com/UA-SETON/8395_1367250748264.mp4'),
        array('http://na.qa.andromeda2.brady.nbs-test.com/emergency-warning-triangle-kit-15245.html','videos.treepodia.com/UA-SETON/164_1367250344446.mp4'),
        array('http://na.qa.andromeda2.brady.nbs-test.com/traffic-cone-sign-full-96185.html','videos.treepodia.com/ua-seton/223_1367250344405.mp4')
        );


    // tests
    public function productVideos(WebGuy $I) {
        $I->wantTo('check the treepodia video');
        $I->expectTo('see product video');

        for ($row = 0; $row < 3; $row++) {
            for ($col = 0; $col < 1; $col++){
                $page = $this->productpage[$row][$col];
                $I->changeBaseURL($page);
                $I->amOnPage('/');
                $I->seeElement('#product-video');
                $I->click('.product-video');
                $I->wait(5);
                $I->switchToWindow();
                $I->moveMouseOver('#video-location');
                $I->click('#video-location');
                $I->wait(10);
                $I->see($this->productpage[$row][$col+1]);
            }

        }

    }

}