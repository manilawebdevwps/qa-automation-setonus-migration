<?php
namespace uat;
use \WebGuy;
use \Codeception\Util\Locator;

class WPSMIG142Cest
{

    /* order history */

    public function _before()
    {
    }

    public function _after()
    {
    }

    public static $product = '/signs/fire-and-exit-signs/exit-signs/in-case-fire-optima-wall-mount-signs.html';
    public static $element_SBWrapper = '#sb-wrapper';
    public static $element_SBProductImage = '.product-image';
    public static $element_SBProductName = '.product-name';
    public static $element_SBProductPrice = '.price';
    public static $element_SBViewBasketButton = '.button.btn-green';
    public static $element_SBContinueShoppingButton = '.button';

    public static $usernameField = '#login-email';
    public static $passwordField = '#login-password';
    public static $loginButton = '//*[@id="login-form"]/div/button';

    public static $username = 'test@emakina.fr';
    public static $password = 'emakina';

    public static $checkout_proceedcheckout = '.button.btn-proceed-checkout.btn-checkout';
    public static $checkout_stepbilling = '#checkout-step-billing';
    public static $checkout_billingshiptothisadd = '#billing:use_for_shipping_yes';
    public static $checkout_billingbutton = '//*[@id="billing-buttons-container"]/button';
    public static $checkout_shippingmethodbutton = '//*[@id="shipping-method-buttons-container"]/button';
    public static $checkout_paymentinformationbutton = '//*[@id="payment-buttons-container"]/button';
    public static $checkout_orderreviewbutton = '//*[@id="review-buttons-container"]/button';
    public static $checkout_stepshippinginfo = '#checkout-step-shipping';
    public static $checkout_stepshippingmethod = '#opc-shipping_method';
    public static $checkout_shippingbestwayground = '#matrixrate';
    public static $checkout_steppaymentinformation = '#checkout-step-payment';
    public static $checkout_invoiceme = '#p_method_purchaseorder';
    public static $checkout_elementorderreview = '#checkout-review-table-wrapper';
    public static $ordernumber = 'html/body/div[1]/div[3]/div[2]/div/div/p[1]/a';

    public static $usernameF = '#email';
    public static $passwordP = '#pass';
    public static $usernamePass = 'test@emakina.fr';
    public static $passwordPass = 'emakina';
    public static $loginF = 'html/body/div[1]/div[3]/div[2]/div/div/form/div/div/div[1]/div/button';
    public static $ordernumberpath = 'html/body/div[3]/div[1]/div[2]/div[1]/div/table/tbody/tr[3]/td[1]';
    public static $orderslink = 'html/body/div[3]/div[1]/div[2]/div[2]/div/div[2]/ul/li[4]/a';
    public static $status = 'html/body/div[3]/div[1]/div[2]/div[1]/div/table/tbody/tr[3]/td[5]/em';
    public static $register = 'html/body/div[1]/div[3]/div[1]/div[1]/div[3]/ul/li/a';
    public static $pdf = 'html/body/div[3]/div[1]/div[2]/div[1]/div/table/tbody/tr[3]/td[6]/a';

    // tests
    public function orderHistory(WebGuy $I) {

        $I->amOnPage(self::$product);
        $I->wantTo('Add product to cart');
        $I->click('Add to Basket');
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(self::$element_SBWrapper, 30); // secs
        $I->waitForElement(self::$element_SBProductImage, 10); // secs
        $I->expectTo('See Product image at the left');
        $I->seeElement(self::$element_SBProductImage);
        $I->expectTo('See Product name and code at the right');
        $I->seeElement(self::$element_SBProductName);
        $I->expectTo('See Product price at the right most');
        $I->seeElement(self::$element_SBProductPrice);
        $I->expectTo('See button for view basket');
        $I->wait(10);
        $I->switchToIFrame('sb-player');
        $I->seeElement(self::$element_SBViewBasketButton);
        $I->seeElement(self::$element_SBContinueShoppingButton);
        $I->wantTo('one page checkout');
        $I->expectTo('proceed checkout');
        $I->click(self::$element_SBViewBasketButton);
        $I->expectTo('See cart page');
        $I->seeInCurrentUrl('checkout/cart/');
        $I->click(self::$checkout_proceedcheckout);
        $I->expectTo('See login page');
        $I->seeInCurrentUrl('checkout/onepage/');
        $I->waitForElement(self::$usernameField, 30); // secs
        $I->waitForElement(self::$passwordField, 10); // secs
        $I->waitForElement(self::$loginButton, 30); // secs
        $I->fillField(self::$usernameField,self::$username);
        $I->fillField(self::$passwordField,self::$password);
        $I->click(self::$loginButton);
        $I->expectTo('See checkout one page');
        $I->seeInCurrentUrl('checkout/onepage/index/');
        $I->expectTo('see billing section');
        $I->seeElement(self::$checkout_stepbilling);
        $I->seeElement(self::$checkout_billingshiptothisadd);
        $I->seeElement(self::$checkout_billingbutton);
        $I->click(self::$checkout_billingshiptothisadd);
        $I->wait(3);
        $I->click(self::$checkout_billingbutton);
        $I->wait(5);
        $I->expectTo('see shipping method section');
        $I->seeElement(self::$checkout_stepshippingmethod);
        $I->wait(5);
        $I->click(self::$checkout_shippingbestwayground);
        $I->click(self::$checkout_shippingmethodbutton);
        $I->wait(10);
        $I->expectTo('see payment information section');
        $I->seeElement(self::$checkout_steppaymentinformation);
        $I->click(self::$checkout_invoiceme);
        $I->click(self::$checkout_paymentinformationbutton);
        $I->expectTo('see order review section');
        $I->wait(3);
        $I->seeElement(self::$checkout_elementorderreview);
        $I->click(self::$checkout_orderreviewbutton);
        $I->wait(5);
        $I->canSeeInCurrentUrl('checkout/onepage/success/');
        $I->click('.account-link');
        $I->wait(5);
        $I->canSeeInCurrentUrl('customer/account/');
        $I->click(self::$orderslink);
        $I->see('Complete',self::$status);
        $I->see('PDF version',self::$pdf);
    }


}