<?php
$I = new WebGuy\CheckoutSteps($scenario);

$I->wantTo('check AMEX/Discovery of Seton US on global platform');
$I->changeBaseURL('http://stage.andromeda2.brady-us.nbs-test.com');  //not allowed to checkout in QA platform
//$I->amOnPage('/signs/fire-exit/exit-signs/in-case-fire-optima-wall-mount-signs-77016.html');
$I->amOnPage(ProductPage::$ProductPageURL);
$I->click('Add to Cart');
sleep(5);
$I->amOnPage('/checkout/onepage/');
$I->click('Checkout as Guest');
$I->wait(3);
$I->canSeeElement('#co-billing-form'); // secs

#Billing
$I->billing(true);
$I->click('#billing-buttons-container .button');

#Shipping
$I->wait(4);
$I->canSeeElement('#co-shipping-method-form'); // secs
$I->click('#shipping-method-buttons-container .button');

#Payment
$I->wait(4);
$I->canSeeElement('#co-payment-form');
$I->checkOption('#p_method_paymetric');
$I->wait(2);
$I->fillField('payment[cc_owner]', 'John Doe');
$rand = rand(0, 1);
$select_cc_type[] = array('American Express', '371449635398431', '1234');
$select_cc_type[] = array('Discover', '6011000990139424', '123');
$I->selectOption('payment[cc_type]', $select_cc_type[$rand][0]);
$I->wait(3);
$I->fillField('payment[cc_number]', $select_cc_type[$rand][1]);
$I->wait(3);
$I->fillField('payment[cc_exp_month]', '01');
$I->fillField('payment[cc_exp_year]', '18');
$I->fillField('payment[cc_cid]', $select_cc_type[$rand][2]);
$I->click('#payment-buttons-container .button');
$I->wait(5);
//$I->canSeeElement('#checkout-agreements'); checkout-step-review
$I->canSeeElement('#checkout-step-review');
//$I->canSeeElement('#review-buttons-container .button');

#order preview - not allowed to checkout
$I->click('#review-buttons-container .button');
$I->wait(20);
$I->canSee('YOUR ORDER HAS BEEN RECEIVED', 'h1');
?>