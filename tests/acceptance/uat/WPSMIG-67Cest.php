<?php
namespace uat;
use \WebGuy;
use Faker\Factory;
use Codeception\Module\MailCatcherHelper;


class WPSMIG67Cest extends \Codeception\Module
{
    /*Quote Request*/

    public $ln='';
    public $fn='';
    public $comp='';
    public $stAd='';
    public $city='';
    public $zip='';
    public $email='';
    public $order_date = '';
    public $deliver_date = '';
    public $formData = array();


    public function _before()
    {
        $faker = Factory::create();

        $this->fn    = $faker->firstName;
        $this->ln    = $faker->lastName;
        $this->comp  = $faker->company;
        $this->stAd  = $faker->streetAddress;
        $this->city  = $faker->city;
        $this->zip   = $faker->postcode;
        $this->email = $faker->email;
        $this->order_date = date('m/d/Y',strtotime("+2 days"));
        $this->deliver_date = date('m/d/Y',strtotime("+5 days"));

        $this->formData = array('Is your quote enquiry regarding',
            'General price &amp; availability',
            'Details of what you would like a quote on',
            'Details of the quote text field test',
            'Have you had a quote from us before ?',
            'Yes',
            'Enter Minimum',
            '1000',
            'Enter Maximum',
            '2000',
            'When are you looking to place an order ?',
            'Other',
            'Order date',
            $this->order_date,
            //'04/26/2014',
            'When is delivery required ?',
            'By date',
            'By date',
            $this->deliver_date,
            //'04/29/2014',
            'What aspects of this quote will influence your decision to place an order ?',
            'Price',
            'Product quality',
            'Other',
            'Quote influences details',
            'Aspect that influence order text field test',
            'Have you had a similar quote from an alternative supplier ?',
            'Yes',
            'Competitor quote details',
            'Competitor quote text field test',
            'Name',
            $this->fn.' '.$this->ln,
            //'Jarrod Gleason',
            'Company',
            $this->comp,
            //'Harber Inc',
            'Street Address',
            $this->stAd,
            //'6995 Shirley Streets',
            'City',
            $this->city,
            //'Kilbackview',
            $this->zip,
            //'99647',
            'New York',
            'Function',
            'Function text field test',
            'Customer contact number :',
            'Phone number',
            '123-456-7890',
            'Email address',
            $this->email,
            //'langworth.joana@yahoo.com',
            'Account number',
            '123456789',
            'Additional information :',
            'additional info text field test',
            'Subscribe user to the newsletter',
            'No',
            'Stock-Destructible-Asset-Labels-59830-lg.jpg'
        );
    }



    public function _after()
    {
        //unset the array
    }

    public static $gmail_host = '{imap.gmail.com:993/imap/ssl}INBOX';
    public static $gmail_username = 'setonus.andromeda@gmail.com';
    public static $gmail_password = 'Setonusandromeda!';

    public static $url_homepagequoterequest = 'quote/request/';
    public static $url_productquoterequest = 'quotation/product/index/sku/97518/';
    public static $product_productquoterequest = '/safety-starts-with-you-safety-slogan-wallcharts.html';

    public static $admin_url = '/index.php/andromeda-bo/';
    public static $admin_username = 'lilybeth_ranada';
    public static $admin_password = '********';
    public static $admin_sales = '//*[@id="nav"]/li[2]/a/span';
    public static $admin_searchResult = '//*[@id="quotationGrid_table"]/tbody/tr/td[3]';


    // tests
    public function SendQuoteFromHeader(WebGuy $I, $fn) {

        // /***************
        // /* HEADER
        // /***************
        $I->wantTo('Fill the form - QUOTE ENQUIRY');
        $I->expectTo('see QUOTE ENQUIRY from header');
        $I->amOnPage('/');
        $I->click('Quote Request');
        $I->expectTo('see quote request form');
        $I->seeInCurrentUrl(self::$url_homepagequoterequest);
        //Customer requested a quote
        //Quote details :
        //Is your quote enquiry regarding   General price & availability
        $I->expectTo('see checkbox is triggered for the inquiry - Is your quote enquiry regarding');
        $I->checkOption('#general');
        #//next is - $I->checkOption('#customised_product');
        //Details of what you would like a quote on "some text"
        $I->expectTo('see text area is filled with inquiry - Details of what you would like a quote on');
        $I->fillField("#quote_details",'Details of the quote text field test');
        //next is - browse a file
        //Have you had a quote from us before ? Yes

        $I->expectTo('see a file is attached');
        //$I->attachFile('input[@type="file"]', 'C:\Users\ranadali\Desktop\Stock-Destructible-Asset-Labels-59830-lg.jpg');
        //$I->click('.input-file.validate-file');
        #$I->attachFile('input[@type="file"]','Stock-Destructible-Asset-Labels-59830-lg.jpg');
        $I->attachFile('input[name="attachment"]','Stock-Destructible-Asset-Labels-59830-lg.jpg');
        //$I->acceptPopup('.attachment');
        //$I->click('Open');
        $I->wait(3);


       $I->expectTo('see checkbox is triggered for the inquiry - Have you had a quote from us before?');
        $I->checkOption('#quote_before_yes');
        //next is -  $I->checkOption('#quote_before_no');

        //Are you working to a budget ? Yes
        $I->expectTo('see checkbox is triggered for the inquiry - Are you working to a budget ?');
        $I->checkOption('#budget_yes');
        //next is -  $I->checkOption('#budget_no');

        //Enter Minimum ($) "1000"
        $I->fillField('#budget_min','1000');
        //Enter Maximum ($) "2000"
        $I->fillField('#budget_max','2000');

        //When are you looking to place an order ?  Other
        $I->expectTo('see checkbox is triggered for the inquiry - When are you looking to place an order ?');
        //Order date    "04/25/2014"
        $I->checkOption('#when_order_other');
        #$order_date = date('d/m/Y',strtotime("+2 days"));
        #$order_date = date('m/d/Y',strtotime("+2 days"));
        $I->wait(2);
        $I->fillField("#order_date",$this->order_date);
        $I->wait(2);
        //next is -  $I->checkOption('#when_order_today');
        //next is -  $I->checkOption('#when_order_week');
        //$I->checkOption('#when_order_month');
        //next is -  $I->checkOption('#when_order_month');
        //$I->checkOption('.field hiddable when_order');

        //When is delivery required ?    By date
        $I->expectTo('see checkbox is triggered for the inquiry - When is delivery required ?');
        //By date   04/30/2014
        $I->checkOption('#when_deliver_date');
        #$deliver_date = date('d/m/Y',strtotime("+5 days"));
        #$deliver_date = date('m/d/Y',strtotime("+5 days"));
        $I->wait(2);
        $I->fillField('#deliver_date',$this->deliver_date); // with error on date time
        $I->wait(2);
        #next is -  $I->checkOption('#when_deliver_24h');
        //next is -  $I->checkOption('#when_deliver_2or3days');
        //next is -  $I->checkOption('#when_deliver_week');

        //What aspects of this quote will influence your decision to place an order ?
        $I->expectTo('see checkbox is triggered for the inquiry - What aspects of this quote will influence?');
        //Price
        $I->checkOption('#quote_influences_price');
        //Product quality
        $I->checkOption('#quote_influences_product_quality');
        //Other
        $I->checkOption('#quote_influences_other');
        //Quote influences details  "some text"
        $I->fillField("#quote_influences_details","Aspect that influence order text field test");
        //next is -  $I->checkOption('#quote_influences_product_availability');
        //next is -  $I->checkOption('#quote_influences_delivery_lead_time');

        //Have you had a similar quote from an alternative supplier ?   Yes
        $I->expectTo('see checkbox is triggered for the inquiry - Have you had a similar quote from an alternative supplier ?');
        //Yes
        $I->checkOption('#competitor_quote_yes');
        //Competitor quote details    "some text"
        $I->fillField("#competitor_quote_details","Competitor quote text field test");
        //next is -  $I->checkOption('#competitor_quote_no');

        //Customer details :
        $I->wantTo('fill fields in  Your Details section');
        //Name
        $I->expectTo('see name field is filled-up');
        $I->fillField('#name',$this->fn.' '.$this->ln);
        //Company
        $I->expectTo('see Company field is filled-up');
        $I->fillField('#company',$this->comp);
        //Street Address
        $I->expectTo('see Street Address field is filled-up');
        $I->fillField('#address',$this->stAd);
        //City
        $I->expectTo('see City field is filled-up');
        $I->fillField('#city',$this->city);

        $I->expectTo('see State/Province');
        $I->selectOption('form select[name=region_id]', 'New York');

        //???
        $I->expectTo('see Postcode field is filled-up');
        $I->fillField('#zip',$this->zip);
        #$I->fillField('#zip','10001');

        //Function
        $I->expectTo('see Function field is filled-up');
        $I->fillField('#function','Function text field test');

        //Customer contact number :
        //Phone number
        $I->wantTo('fill fields of contact number'); //314-590-0900
        #$I->fillField('#telephone',$faker->phoneNumber);
        $I->fillField('#telephone','123-456-7890');
        //Email address
        $I->expectTo('see Email address field is filled-up');
        $I->fillField('#email',$this->email);
        //Account number
        $I->expectTo('see Account number field is filled-up');
        $I->fillField('#account_number','123456789');

        //Additional information :
        //Additional information    "some text"
        $I->wantTo('fill fields of Additional information section');
        $I->expectTo('see Additional information section');
        $I->expectTo('see Additional information field is filled-up');
        $I->fillField('#additional_information','additional info text field test');
        //Subscribe user to the newsletter  No
        $I->expectTo('see no newsletter will be sent to me');
        $I->checkOption('#no_newsletter');
        $I->wantTo('send now the quote to  Customer Quoting Group ');
        $I->expectTo('Submit the form');
        $I->click('Submit Request');
        $I->waitForElementVisible('.success-msg',3);
        $I->wantTo('See email content');
        $I->expectTo('see email content');

        // /*** Email Body **
        if ($I->see('Your request was submitted and will be responded to as soon as possible.')){
        $I->wait(30);

        foreach ($this->formData as $value){
                $I->useIMAP($value);
                }
                //assert each of the message body
        }//If
    }


    public function SendQuoteFromProductPage(WebGuy $I) {
        // /***************
        // /* PRODUCT PAGE
        // /***************
       $I->wantTo('Fill the form - Your Information');
        $I->expectTo('see QUOTE ENQUIRY from product page');
        $I->amOnPage(self::$product_productquoterequest);
        $I->click('#product-quotation');
        $I->wait(5);
        $I->expectTo('see quote request form');
        $I->seeInCurrentUrl(self::$url_productquoterequest);
        $I->expectTo('see Product Quotation - Your Information');
        //$I->selectOption('form select[name=civility]', 'MS');
        $I->expectTo('see name field is filled-up');
        $I->fillField('#name',$this->fn.' '.$this->ln);
        //Company
        $I->expectTo('see Company field is filled-up');
        $I->fillField('#company',$this->comp);
        //Street Address
        $I->expectTo('see Street Address field is filled-up');
        $I->fillField('#address',$this->stAd);
        $I->expectTo('see Postcode field is filled-up');
        $I->fillField('#postcode',$this->zip);
        #$I->fillField('#zip','10001');
        //City
        $I->expectTo('see City field is filled-up');
        $I->fillField('#city',$this->city);

        $I->expectTo('see State/Province');
        $I->selectOption('form select[name=region_id]', 'New York');
        //Email address
        $I->expectTo('see Email address field is filled-up');
        $I->fillField('#email',$this->email);
        //Customer contact number :
        //Phone number
        $I->wantTo('fill fields of contact number');
        #$I->fillField('#telephone',$faker->phoneNumber);
        $I->fillField('#telephone','1234567890');
        //Remarks
        $I->expectTo('see Remark field is filled-up');
        $I->fillField('#remarks','Remarks text field test');
        $I->wantTo('send now the quote to  Customer Quoting Group ');
        $I->expectTo('Submit the quotation');
        $I->click('Submit quotation');
        $I->waitForElementVisible('.success-msg',5);
        $I->wantTo('See email content');
        $I->expectTo('see email content');
         // /*** Email Body **
         if ($I->see('The quotation was sent.')){
             $I->wait(10);
            $I->amOnPage(self::$admin_url);
             $I->fillField('#username',self::$admin_username);
             $I->fillField('#login',self::$admin_password );
             $I->Click('Login');
             $I->wait(10);
             $I->seeInCurrentUrl(self::$admin_url);
             $I->Click(self::$admin_sales);
             $I->wait(5);
             $I->Click('Quotations');
             $I->fillField('#quotationGrid_filter_email',$this->email);
             #$I->fillField('#quotationGrid_filter_email',$this->email);
             $I->Click('Search');
             $I->wait(5);
             $I->Click(self::$admin_searchResult);
             $I->wait(5);
             $I->see($this->fn.' '.$this->ln);
             $I->see($this->comp);
             $I->see($this->stAd);
             $I->see($this->zip);
             $I->see($this->city);
             $I->see($this->email);
             $I->see('1234567890');
             $I->see('97518');
             $I->see('Remarks text field test');
          }//If
    }//function SendQuoteFromProductPage


    public function SendQuoteFromFooter(WebGuy $I) {
        // /***************
        // /*    FOOTER
        // /***************
        $I->wantTo('send a quote to Customer Quoting Group from the footer section');
        $I->expectTo('see QUOTE ENQUIRY from the footer');
        $I->amOnPage('/');
        $I->click('Request a Quote');
        $I->expectTo('see quote request form');
        $I->seeInCurrentUrl(self::$url_homepagequoterequest);
        $faker = Factory::create();
        //Customer requested a quote
        //Quote details :
        //Is your quote enquiry regarding   General price & availability
        $I->expectTo('see checkbox is triggered for the inquiry - Is your quote enquiry regarding');
        $I->checkOption('#general');
        #//next is - $I->checkOption('#customised_product');
        //Details of what you would like a quote on "some text"
        $I->expectTo('see text area is filled with inquiry - Details of what you would like a quote on');
        $I->fillField("#quote_details",'Details of the quote text field test');
        //next      //next is - browse a file
        //Have you had a quote from us before ? Yes
        $I->expectTo('see checkbox is triggered for the inquiry - Have you had a quote from us before?');
        $I->checkOption('#quote_before_yes');
        //next is -  $I->checkOption('#quote_before_no');

        //Are you working to a budget ? Yes
        $I->expectTo('see checkbox is triggered for the inquiry - Are you working to a budget ?');
        $I->checkOption('#budget_yes');
        //next is -  $I->checkOption('#budget_no');

        //Enter Minimum ($) "1000"
        $I->fillField('#budget_min','1000');
        //Enter Maximum ($) "2000"
        $I->fillField('#budget_max','2000');

        //When are you looking to place an order ?  Other
        $I->expectTo('see checkbox is triggered for the inquiry - When are you looking to place an order ?');
        //Order date    "04/25/2014"
        $I->checkOption('#when_order_other');
        #$order_date = date('d/m/Y',strtotime("+2 days"));
        //     $order_date = date('m/d/Y',strtotime("+2 days"));
        $I->wait(2);
        $I->fillField("#order_date",$this->order_date);
        $I->wait(2);
        //next is -  $I->checkOption('#when_order_today');
        //next is -  $I->checkOption('#when_order_week');
        //$I->checkOption('#when_order_month');
        //next is -  $I->checkOption('#when_order_month');
        //$I->checkOption('.field hiddable when_order');

        //When is delivery required ?    By date
        $I->expectTo('see checkbox is triggered for the inquiry - When is delivery required ?');
        //By date   04/30/2014
        $I->checkOption('#when_deliver_date');
        #$deliver_date = date('d/m/Y',strtotime("+5 days"));
        ///      $deliver_date = date('m/d/Y',strtotime("+5 days"));
        $I->wait(2);
        $I->fillField('#deliver_date',$this->deliver_date); // with error on date time
        $I->wait(2);
        #next is -  $I->checkOption('#when_deliver_24h');
        //next is -  $I->checkOption('#when_deliver_2or3days');
        //next is -  $I->checkOption('#when_deliver_week');

        //What aspects of this quote will influence your decision to place an order ?
        $I->expectTo('see checkbox is triggered for the inquiry - What aspects of this quote will influence?');
        //Price
        $I->checkOption('#quote_influences_price');
        //Product quality
        $I->checkOption('#quote_influences_product_quality');
        //Other
        $I->checkOption('#quote_influences_other');
        //Quote influences details  "some text"
        $I->fillField("#quote_influences_details","Aspect that influence order text field test");
        //next is -  $I->checkOption('#quote_influences_product_availability');
        //next is -  $I->checkOption('#quote_influences_delivery_lead_time');

        //Have you had a similar quote from an alternative supplier ?   Yes
        $I->expectTo('see checkbox is triggered for the inquiry - Have you had a similar quote from an alternative supplier ?');
        //Yes
        $I->checkOption('#competitor_quote_yes');
//        //Competitor quote details    "some text"
        $I->fillField("#competitor_quote_details","Competitor quote text field test");
        //next is -  $I->checkOption('#competitor_quote_no');

        //Customer details :
        $I->wantTo('fill fields in  Your Details section');
        //Name
        $I->expectTo('see name field is filled-up');
        $I->fillField('#name',$this->fn.' '.$this->ln);
        //Company
        $I->expectTo('see Company field is filled-up');
        $I->fillField('#company',$this->comp);
        //Street Address
        $I->expectTo('see Street Address field is filled-up');
        $I->fillField('#address',$this->stAd);
        //City
        $I->expectTo('see City field is filled-up');
        $I->fillField('#city',$this->city);

        $I->expectTo('see State/Province');
        $I->selectOption('form select[name=region_id]', 'New York');

        //???
        $I->expectTo('see Postcode field is filled-up');
        $I->fillField('#zip',$this->zip);
        #$I->fillField('#zip','10001');

        //Function
        $I->expectTo('see Function field is filled-up');
        $I->fillField('#function','Function text field test');

        //Customer contact number :
        //Phone number
        $I->wantTo('fill fields of contact number'); //314-590-0900
        #$I->fillField('#telephone',$faker->phoneNumber);
        $I->fillField('#telephone','123-456-7890');
        //Email address
        $I->expectTo('see Email address field is filled-up');
        $I->fillField('#email',$this->email);
        //Account number
        $I->expectTo('see Account number field is filled-up');
        $I->fillField('#account_number','123456789');

        //Additional information :
        //Additional information    "some text"
        $I->wantTo('fill fields of Additional information section');
        $I->expectTo('see Additional information section');
        $I->expectTo('see Additional information field is filled-up');
        $I->fillField('#additional_information','additional info text field test');
        //Subscribe user to the newsletter  No
        $I->expectTo('see no newsletter will be sent to me');
        $I->checkOption('#no_newsletter');
        $I->wantTo('send now the quote to  Customer Quoting Group ');
        $I->expectTo('Submit the form');
        $I->click('Submit Request');
        $I->waitForElementVisible('.success-msg',3);
        $I->wantTo('See email content');
        $I->expectTo('see email content');

        // /*** Email Body **
        if ($I->see('Your request was submitted and will be responded to as soon as possible.')){
            $I->wait(30);

            foreach ($this->formData as $value){
                $I->useIMAP($value);
            }
            //assert each of the message body
        }//If

    }

}
