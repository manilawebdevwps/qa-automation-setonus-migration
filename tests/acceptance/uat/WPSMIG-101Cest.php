<?php
namespace uat;
use \WebGuy;

use Faker\Factory;

class WPSMIG101Cest
{
    /* Live Chat*/

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function liveChat(WebGuy $I) {
        $I->wantTo('chat with customer service');
        $I->amOnPage('/');
        $I->click('.extendable-container');
        $I->click('.extendable');
        $I->wait(10);
        $I->expectTo('See chat window');
        $I->switchToWindow('chat55631553');
        $I->switchToIFrame('initialtextFrame');
        $faker = Factory::create();
        $I->fillField('#q2', $faker->firstName);
        $I->fillField('#q3', $faker->email);
        $I->fillField('#q6', 'test');
        $I->wait(5);
        $I->switchToWindow('chat55631553');
        $I->click('Submit survey');
        $I->switchToWindow();
    }

}