<?php
use \WebGuy;

class CreateAccountCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CreateAccount(Webguy $webGuy, $scenario){
        $I = new WebGuy\UserSteps($scenario);
        $I->wantTo('Test registration of Seton US for global platform');
        $I->changeBaseURL(LoginPage::$BaseURL_https);
        $I->amOnPage(LoginPage::$URL_createAccount);
        $I->register();
        $I->click('Submit');
        sleep(3);
        $I->see('Thank you for registering with Seton US');
    }

}