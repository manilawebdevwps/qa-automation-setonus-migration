<?php
use \WebGuy;

class CheckoutAsGuestCestCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckoutAsGuest(WebGuy $webGuy, $scenario){
        $scenario->skip('Checkout occurs pop error.');
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->amOnPage(ProductPage::$ProductPageURL);
        $I->wantTo('Add product to cart');
        $I->click(ProductPage::$element_ButtonCart);
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(ProductPage::$element_SBWrapper, 30); // secs
        sleep(5);
        $I->amOnPage('/checkout/onepage/');
        $I->see('Checkout as Guest');
        $I->click('Checkout as Guest');
        $I->wait(3);
        $I->canSeeElement('#co-billing-form'); // secs
        #Billing
        $I->billing(true);
        $I->click('#billing-buttons-container .button');
        #Shipping
        $I->wait(4);
        $I->canSeeElement('#co-shipping-method-form'); // secs
        $I->click('#shipping-method-buttons-container .button');
        #Payment
        $I->wait(4);
        $I->canSeeElement('#co-payment-form');
        $I->checkOption('#p_method_paymetric');
        $I->wait(2);
        $I->fillField('payment[cc_owner]', 'John Doe');
        $rand = rand(0, 1);
        $select_cc_type[] = array('American Express', '371449635398431', '1234');
        $select_cc_type[] = array('Discover', '6011030995200004', '123');
        $I->selectOption('payment[cc_type]', $select_cc_type[$rand][0]);
        $I->wait(3);
        $I->fillField('payment[cc_number]', $select_cc_type[$rand][1]);
        $I->wait(3);
        $I->fillField('payment[cc_exp_month]', '01');
        $I->fillField('payment[cc_exp_year]', '18');
        $I->fillField('payment[cc_cid]', $select_cc_type[$rand][2]);
        $I->click('#payment-buttons-container .button');
        $I->wait(4);
        $I->canSeeElement('#checkout-agreements');
        #order preview
        $I->click('#review-buttons-container .button');
        $I->wait(10);
        $I->canSee('YOUR ORDER HAS BEEN RECEIVED', 'h1');
    }
}