<?php
use \WebGuy;

class ForgotPasswordCest
{

    // tests
    public function LoginPage(WebGuy $I){
        $I->amOnPage('/');
        $I->wantTo('To check login page is displaying');
        $I->amOnPage(ProductPage::route('customer/account/login/'));
    }	
	
	public function ForgotPasswordPage(WebGuy $I) {
		$I->amOnPage(ProductPage::route('customer/account/login/'));
		$I->wantTo('To check forgot password page is displaying');
		$I->click(".buttons-set a");
		$I->expectTo('See Forgot Password header title');
		$I->see('Forgot Your Password?');
	}
	
	public function InvalidPassword(WebGuy $I) {
		$I->amOnPage(ProductPage::route('customer/account/forgotpassword/'));
		$I->wantTo('To check password validation');
		$I->fillField('#email_address','invalidpassword');
		$I->click(".buttons-set .button");
		$I->see('Please enter a valid email address');
	}
	
	public function SendPassword(WebGuy $I) {
		$I->amOnPage(ProductPage::route('customer/account/forgotpassword/'));
		$I->wantTo('To check forgot password submission');
		$I->fillField('#email_address','andy_homalayan@bradycorp.com');
		$I->click(".buttons-set .button");
		$I->wait(5);
		$I->see('If there is an account associated with andy_homalayan@bradycorp.com you will receive an email with a link to reset your password.');
	}

}