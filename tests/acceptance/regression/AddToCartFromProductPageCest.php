<?php
use \WebGuy;

class AddToCartFromProductPageCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function ProductPage(WebGuy $I){
        $I->amOnPage('/');
        $I->wantTo('To check Product is displaying providing id of the product');
        $I->amOnPage(ProductPage::$ProductPageURL);
    }
    public function AddProduct(WebGuy $I){
        $I->amOnPage(ProductPage::$ProductPageURL);
        $I->wantTo('Add product to cart');
        $I->click(ProductPage::$element_ButtonCart);
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(ProductPage::$element_SBWrapper, 30); // secs
        $I->waitForElement(ProductPage::$element_SBProductImage, 10); // secs
        $I->expectTo('See Product image at the left');
        $I->seeElement(ProductPage::$element_SBProductImage);
        $I->expectTo('See Product name and code at the right');
        $I->seeElement(ProductPage::$element_SBProductName);
        $I->expectTo('See Product price at the right most');
        $I->seeElement(ProductPage::$element_SBProductPrice);
        $I->expectTo('See button for view basket');
        $I->wait(10);
        $I->switchToIFrame('sb-player');
        $I->seeElement(ProductPage::$element_SBViewBasketButton);
        $I->expectTo('Continue shopping');
        $I->seeElement(ProductPage::$element_SBContinueShoppingButton);

    }
    public function Cart_ViewBasket(WebGuy $I){
        $I->amOnPage(ProductPage::$ProductPageURL);
        $I->wantTo('Add product to cart and my Cart');
        $I->click(ProductPage::$element_ButtonCart);
        $I->expectTo('See overlay is displayed');
        $I->waitForElement(ProductPage::$element_SBWrapper, 30); // secs
        $I->wait(15);
        $I->switchToIFrame('sb-player');
        $I->amGoingTo('Click the view basket button');
        $I->click(ProductPage::$element_SBViewBasketButton);
        $I->wait(15);
        $I->expectTo('See Cart Page');
        $I->seeCurrentUrlEquals(ProductPage::route('/checkout/cart/'));
        $I->expectTo('See H1 text');
        $I->seeElement(ProductPage::$element_CheckoutBasketTitle);
        $I->expectTo('See Cart Table');
        $I->seeElement(ProductPage::$element_CheckoutCartTable);
    }

}