<?php
use \WebGuy;

class QuickOrderCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function QuickOrderPage(WebGuy $I) {
    	$I->amOnPage('/');
        $I->wantTo('Check Quick Order page is displaying');
        $I->click("#quickorderHeaderLink a");
		$I->expectTo('See Quick Order header title');
		$I->see('Quick Order');
    }

    public function InvalidSku(WebGuy $I) {
        $I->amOnPage('quick-shop.html');
        $I->wantTo('Check if not existent SKU is validated');
        $I->fillField('#product-item-0','ABC123');
        $I->wait(5);
        $I->expectTo('See validation error');
        $I->seeElement('.validation-failed');
    }

    public function Autocomplete(WebGuy $I) {
        $I->amOnPage('quick-shop.html');
        $I->wantTo('Check if autocomplete or type ahead is working');
        $I->fillField('#product-item-0','86');
        $I->wait(5);
        $I->expectTo('See product overlay is displayed');
        $I->see('Exit - Glow-In-The-Dark Polished Red Sign');
    }

    public function AddToTable(WebGuy $I) {
        $I->amOnPage('quick-shop.html');
        $I->wantTo('Check if Product is added to Table');
        $I->fillField('#product-item-0','86');
        $I->wait(5);
        $I->click('Exit - Glow-In-The-Dark Polished Red Sign');
        $I->wait(5);
        $I->dontSeeElement('#product-id-141814');    
    }

    public function AddToCart(WebGuy $I) {
        $I->amOnPage('quick-shop.html');
        $I->wantTo('Check if Product is added to Cart');
        $I->fillField('#product-item-0','86');
        $I->wait(5);
        $I->click('Exit - Glow-In-The-Dark Polished Red Sign');
        $I->wait(5); 
        $I->click('.cart .btn-cart');
        $I->wait(5); 
        $I->expectTo('see confirmation text that has been added');
        $I->see('References have been added to your shopping cart.');
    }

}