<?php
namespace WebGuy;

use Faker\Factory;

class CheckoutSteps extends \WebGuy
{
    function billing($same_shipping = true)
    {
        $I = $this;
        $faker = Factory::create();
        $I->fillField('billing[firstname]', $faker->firstName);
        $I->fillField('billing[lastname]', $faker->lastName);
        $I->fillField('billing[email]', $faker->email);        
        $I->fillField('billing[company]', 'Brady');        
        $I->fillField('billing[street][]', '123 Main Street');        
        $I->fillField('billing[city]', 'Beverly Hills');        
        $I->selectOption('billing[region_id]', 'California');                        
        $I->fillField('billing[postcode]', '90210'); 
    }//end function    
}