<?php
namespace WebGuy;

use Faker\Factory;

class UserSteps extends \WebGuy
{

    function login($name, $password)
    {
        $I = $this;
        $I->amOnPage(\LoginPage::$URL);
        $I->fillField(\LoginPage::$usernameField, $name);
        $I->fillField(\LoginPage::$passwordField, $password);
        $I->click(\LoginPage::$loginButton);
    }
    
    //Sign Up for Mailing List in Top Header Navigator
    function signUp(){
        $I = $this;
        $faker = Factory::create();
        $I->fillField('firstname',$faker->firstName);
        $I->fillField('lastname',$faker->lastName);
        $I->fillField('email_su',$faker->email);
        $I->click('.es-form-btn');
    }

    function liveChat(){
        $I = $this;
        $faker = Factory::create();
        $I->fillField('fname', $faker->firstName);
        $I->fillField('optionaldata5', $faker->email);
        $I->fillField('optionaldata4','Sample Question for Automation');
        $I->click('Submit');
    }
    
    
    function register()
    {
        $I = $this;
        $faker = Factory::create();
        $I->fillField('firstname', $faker->firstName);
        $I->fillField('lastname', $faker->lastName);
        $I->fillField('email', $faker->email);        
        $I->fillField('password', '123123');        
        $I->fillField('confirmation', '123123');        
        $I->fillField('company', 'Brady');        
        $I->fillField('telephone', '123-465-7890');  
        $I->fillField('street[]', '123 Main Street'); 
        $I->fillField('city', 'Beverly Hills'); 
        //$I->fillField('state', 'California'); 
        $I->selectOption('region_id', 'California');
        $I->fillField('postcode', '90210'); 
    }//end function      
}